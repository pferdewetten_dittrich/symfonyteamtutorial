# Simplest Symfony tutorial

**Table of contents**

* [Task 1](#task-1)
* [Task 2](#task-2)
* [Task 3](#task-3)
* [Task 4](#task-4)

## Preparation:
* create project Folder
* Setup mini docker container with compose


## Task 1: <a id="task-1" href="#task-1">#</a>
* Install symfony
* Remove git components from symfony folder

## Task 2: <a id="task-2" href="#task-2">#</a>
* Create first route
* Add route parameter and use for response

## Task 3: <a id="task-3" href="#task-3">#</a>
* Install swagger
* Create swagger config for route

## Task 4: <a id="task-4" href="#task-4">#</a>
* Setup Twig
* Create template
* Use template for response
